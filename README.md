# Viz Mock #

[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

Viz Mock is a full stack web application enabling users to create robust datasets using natural human inputs.

## Getting Started

General disclaimer.. I'm using a Windows 10 (x64) machine for development. At the moment the npm scripts are using commands specific to Windows command prompt. If you're using another architecture or OS, *please* feel free to contribute! ;)

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You will need to install [MongoDB](https://docs.mongodb.com/manual/). I am using MongoDB 3.4.5 2008R2Plus SSL (64 bit).

You will also need to add the path to the `bin/mongod.exe` file to your Windows PATH environment variable. For example, C:\Program Files\MongoDB\Server\3.4\bin.

### Installing

First, clone the repository by executing 

```
git clone https://bitbucket.org/alogwe/viz-mock.git
```

Then to install all dependencies, change directory to the project directory

```
cd viz-mock
```

and execute

```
npm install
```

**NOTE**: If you have problems installing mongodb drivers during `npm install` please see [npmjs.com/package/mongodb](https://www.npmjs.com/package/mongodb)

The installation process will take a little while. Why not take a few minutes to meditate while you wait?

[All it takes is 10 mindful minutes - Andy Puddicombe](https://www.youtube.com/watch?v=qzR62JJCMBQ)

Welcome back! Woohoo, hopefully everything has installed successfully and you're feeling great! :)

Also, if you did watch that, Andy Puddicombe has a free meditation app here: [Headspace](https://www.headspace.com/headspace-meditation-app)

[//]: # (TODO: End with an example of getting some data out of the system or using it for a little demo)

## Running

To run the project:

1. Open the command prompt or powershell.

2. Navigate to the project directory.

3. Execute `npm start`.

4. Open a web browser, and go to `http://localhost:3000`.

5. Once you're finished running the application, *always* shutdown the MongoDB server by executing `npm stop`. If you use another method you could risk corrupting database files or losing information.

## Testing

[//]: # (TODO: Explain how to run the automated tests for this system)

### End to End Tests

[//]: # (TODO: Explain what these tests test and why, give examples wrapped in ``` emphasis blocks)

### Coding Style Tests

[//]: # (TODO: Implement and mention StandardJS lib)

## Deployment

[//]: # (TODO: Add additional notes about how to deploy this on a live system)

## Built With

[//]: # (TODO: List frameworks/libs used below, with links to their repositories and author information)

## Versioning

[//]: # (TODO: List versioning information. SemVer?)

## License
This product is licensed under the GNU GPLv3 license. See LICENSE.md for complete license details.