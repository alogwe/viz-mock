module.exports = function (grunt) {
  'use strict'

  // Project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: {
      logs: {
        files: [{
          src: ['*.log']
        }]
      },
      reports: {
        files: [{
          src: ['reports/*']
        }]
      },
      screenshots: {
        files: [{
          src: ['reports/screenshots/*']
        }]
      }
    },

    connect: {
      server: {
        options: {
          port: 8001,
          base: '.'
        }
      }
    },

    qunit: {
      all: {
        options: {
          urls: [
            'http://localhost:8001/test/unit/qUnit.html'
          ]
        }
      }
    },

    nightwatch: {
      options: {
        start_process: false,
        standalone: true,
        jarVersion: '3.4.0',
        jar_path: './node_modules/selenium-standalone/.selenium/selenium-server/3.4.0-server.jar',
        src_folders: ['test/e2e'],
        output_folder: './reports', // reports (test outcome) output by nightwatch
        test_settings: {
          'default': {
            'screenshots': {
              'enabled': true, // if you want to keep screenshots
              'path': './reports/screenshots' // save screenshots here
            },
            'globals': {
              'waitForConditionTimeout': 5000 // sometimes internet is slow so wait.
            }
          },
          phantom: {
            'desiredCapabilities': {
              'browserName': 'phantomjs',
              'javascriptEnabled': true,
              'acceptSslCerts': true
            },
            'cli_args': {
              'phantomjs.binary.path': './node_modules/phantomjs-prebuilt/lib/phantom/bin/phantomjs.exe',
              'javascriptEnabled': true // set to false to test progressive enhancement,
            }
          },
          chrome: {
            'desiredCapabilities': {
              'browserName': 'chrome'
            },
            'cli_args': {
              'webdriver.chrome.driver': './node_modules/selenium-standalone/.selenium/chromedriver/2.30-x64-chromedriver',
              'javascriptEnabled': true // set to false to test progressive enhancement
            }
          },
          firefox: {
            'desiredCapabilities': {
              'browserName': 'firefox',
              'marionette': true
            },
            'cli_args': {
              'webdriver.gecko.driver': './node_modules/selenium-standalone/.selenium/geckodriver/0.17.0-x64-geckodriver',
              'javascriptEnabled': true // set to false to test progressive enhancement
            }
          },
          ie: {
            'desiredCapabilities': {
              'browserName': 'internet explorer',
              'javascriptEnabled': true,
              'acceptSslCerts': true,
              'allowBlockedContent': true,
              'ignoreProtectedModeSettings': true
            },
            'cli_args': {
              'webdriver.ie.driver': './node_modules/selenium-standalone/.selenium/iedriver/3.4.0-x64-IEDriverServer.exe',
              'javascriptEnabled': true // set to false to test progressive enhancement
            }
          }
        }
      }
    },

    selenium_standalone: {
      options: {
        stopOnExit: true
      },
      your_target: {
        seleniumDownloadURL: 'https://selenium-release.storage.googleapis.com',
        seleniumVersion: '3.4.0',
        drivers: {
          chrome: {
            version: '2.30',
            arch: 'x64',
            baseURL: 'https://chromedriver.storage.googleapis.com'
          },
          firefox: {
            version: '0.17.0',
            arch: 'x64',
            baseURL: 'https://github.com/mozilla/geckodriver/releases/download'
          },
          ie: {
            version: '3.4.0',
            arch: 'x64',
            baseURL: 'https://selenium-release.storage.googleapis.com'
          }
        }
      }
    }
  })

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  // By default, lint and run all tests.
  // grunt.registerTask('default', ['clean', 'selenium_standalone'])

  // // A convenient task alias.
  // grunt.registerTask('qUnit-test', ['connect', 'qunit'])

  var target = grunt.option('target') || 'chrome'
  grunt.registerTask('e2etest', ['connect', 'nightwatch:' + target])
}
