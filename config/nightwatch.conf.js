require('env2')('./config/nightwatch.env') // optionally store your environment variables in .env
const SCREENSHOT_PATH = './reports/screenshots/'
const BINPATH = './node_modules/nightwatch/bin/'

// we use a nightwatch.conf.js file so we can include comments and helper functions
module.exports = {
  'src_folders': [
    'test/e2e'// Where you are storing your Nightwatch e2e tests
  ],
  'output_folder': './reports', // reports (test outcome) output by nightwatch
  'selenium': { // downloaded by selenium-download module
    'start_process': true, // tells nightwatch to start/stop the selenium process
    'server_path': './node_modules/nightwatch/bin/selenium.jar',
    'host': '127.0.0.1',
    'port': 4444, // standard selenium port
    'cli_args': {
      'webdriver.chrome.driver': BINPATH + 'chromedriver',
      'webdriver.gecko.driver': BINPATH + 'geckodriver',
      'webdriver.edge.driver': BINPATH + 'edgedriver'
    }
  },
  'test_settings': {
    'default': {
      'screenshots': {
        'enabled': true, // if you want to keep screenshots
        'path': SCREENSHOT_PATH // save screenshots here
      },
      'globals': {
        'waitForConditionTimeout': 5000 // sometimes internet is slow so wait.
      },
      'desiredCapabilities': { // use Chrome as the default browser for tests
        'browserName': 'chrome',
        'javascriptEnabled': true
      }
    },
    'chrome': {
      'desiredCapabilities': {
        'browserName': 'chrome',
        'javascriptEnabled': true
      }
    },
    'firefox': {
      'desiredCapabilities': {
        'browserName': 'firefox',
        'marionette': true,
        'javascriptEnabled': true
      }
    },
    'edge': {
      'desiredCapabilities': {
        'browserName': 'MicrosoftEdge'
      }
    }
  }
}

function padLeft (count) { // theregister.co.uk/2016/03/23/npm_left_pad_chaos/
  return count < 10 ? '0' + count : count.toString()
}

var FILECOUNT = 0 // "global" screenshot file count
/**
 * The default is to save screenshots to the root of your project even though
 * there is a screenshots path in the config object above! ... so we need a
 * function that returns the correct path for storing our screenshots.
 * While we're at it, we are adding some meta-data to the filename, specifically
 * the Platform/Browser where the test was run and the test (file) name.
 */
function imagePath (browser, filename) {
  var a = browser.options.desiredCapabilities
  var meta = [a.platform]
  meta.push(a.browserName ? a.browserName : 'any')
  meta.push(a.version ? a.version : 'any')
  meta.push(a.name) // this is the test filename so always exists.
  meta.push(filename || '')
  var metadata = meta.join('~').toLowerCase().replace(/ /g, '')
  return SCREENSHOT_PATH + metadata + '_' + padLeft(FILECOUNT++) + '_'
}

module.exports.imagePath = imagePath
module.exports.SCREENSHOT_PATH = SCREENSHOT_PATH
