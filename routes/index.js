var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Viz Mock - Visual Data Design Tool', greeting: 'Hello, this will be the main landing page.' })
})

module.exports = router
