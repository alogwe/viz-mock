// TODO: Copy and rewrite this test for loading the homepage of the app
var config = require('../../config/nightwatch.conf.js')
var imagePath = config.imagePath

module.exports = { // adapted from: https://git.io/vodU0
  'Guinea Pig Assert Title': function (browser) {
    browser
      .url('https://saucelabs.com/test/guinea-pig')
      .waitForElementVisible('body')
      .assert.title('I am a page title - Sauce Labs')
      .saveScreenshot(imagePath(browser, 'guinea-pig-test.png'))
      .end()
  }
}
